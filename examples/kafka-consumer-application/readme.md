# Kafka-Consumer-Application

This example is a small application reading records from kafka with a KafkaConsumer.

## Installation

### Prerequisites

- [Docker](https://https://www.docker.com/resources/what-container) installed
- Java 8+ installed

### 1. Clone the repository

and go to the ````~/examples/kafka-consumer-application```` directory.

Then build the program:

````bash
gradle build
````

### 2. Get Confluent Platform

Launch the Confluent Platform by runnig:

````bash
docker-compose up -d
````

### 3. Create a topic

First open a shell on the broker docker container.
Open a  new terminal window then run this command:

````bash
docker-compose exec broker bash
````

Use the following command to create the topic:

````bash
kafka-topics --create --topic input-topic --bootstrap-server broker:9092 --replication-factor 1 --partitions 1
````

Keep this terminal window open.

### 4. Compile and run the KafkaConsumer program

Go to the first terminal and run:

````bash
./gradlew shadowJar
````

Now that you have an uberjar for the KafkaConsumer application, you can launch it locally. When you run the following, the prompt won’t return, because the application will run until you exit it. There is always another message to process, so streaming applications don’t exit until you force them.

````bash
java -jar build/libs/kafka-consumer-application-standalone-0.0.1.jar configuration/dev.properties
````

### 5. Produce sample data to the input topic

Using the terminal window you opened in step three, run the following command to start a console-producer:

````bash
kafka-console-producer --topic input-topic --bootstrap-server broker:9092
````

Put some sample data. Each line represents input data for the KafkaConsumer application:

````bash
One morning, 
when Gregor Samsa woke from troubled dreams, 
he found himself transformed in his bed into a horrible vermin. 
He lay on his armour-like back, 
and if he lifted his head a little he could see his brown belly, 
slightly domed and divided by arches into stiff sections.
````

To exit the producer use ````[Ctrl + C]````.

### 6. Inspect the consumed records

Your consumer application should have consumed all the records sent and written them out to a file.

In a new terminal, run this command to print the results to the console:

````bash
cat consumer-records.out
````

You should see the data.
