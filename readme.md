# Overview

## Kafka

[Kafka](https://kafka.apache.org/intro) is a distributed system consisting of servers and clients that communicate via a high-performance [TCP network protocol](https://https://kafka.apache.org/protocol.html). It can be deployed on bare-metal hardware, virtual machines, and containers in on-premise as well as cloud environments.

### Get Started with Kafka

#### Prerequisites

- Linux Operating System: Debian 8, Debian 9, Ubuntu 16.04 LTS or Ubuntu 18.04 LTE
- Java 8+ installed

To start with Kafka follwow the [Apache Kafka Quickstart](https://https://kafka.apache.org/quickstart) instructions.

## Confluent

[Confluent Platform](https://https://docs.confluent.io/platform/current/overview.html) is a full-scale event streaming platform that enables you to easily access, store, and manage data as continuous, real-time streams and is build on Apache Kafka.

### Get Started with Confluent

To start with Confluent use one of the [Quick Start for Apache Kafka](https://https://docs.confluent.io/platform/current/quickstart/cos-quickstart.html) instructions.

## Docker

Docker is a container technology that was launch as an open source project in 2013. More about the technology in [What is a Container?](http://https://www.docker.com/resources/what-container)

### Get started with Docker

To start with Docker follow the [Getting Started](https://https://www.docker.com/get-started) instructions.
